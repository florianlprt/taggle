/**
 * Keyboard
 */

const ENTER = 13;
const BACKSPACE = 8;
const SHORTCUTS = {
    '96': 0,
    '97': 1,
    '98': 2,
    '99': 3,
    '100': 4,
    '101': 5,
    '102': 6,
    '103': 7,
    '104': 8,
    '105': 9,
    '65': 10,
    '66': 11,
    '67': 12,
    '68': 13,
    '69': 14,
    '70': 15,
    '71': 16,
    '72': 17,
    '73': 18,
    '74': 19
};

/**
 * DOM
 */

const BTN_SAVE = document.querySelector('#save');
const BTN_TAGS = document.querySelectorAll('.tag');

/**
 * Events
 */

BTN_SAVE.onclick = save;
BTN_TAGS.forEach((tag) => {
    tag.onclick = (evt) => {
        const { target } = evt;
        toggleTag(target.nodeName === 'SPAN' ? target.parentNode : target);
    };
});

window.onkeyup = ({ which }) => {
    if (which === ENTER) {
        return save();
    } 
    if (which === BACKSPACE) {
        return discard();
    }
    if (which in SHORTCUTS) {
        return toggleTag(BTN_TAGS[SHORTCUTS[which]]);
    }
};

/**
 * Functions
 */

function initTags(tags) {
    BTN_TAGS.forEach((tag) => {
        tag.classList.remove('btn-primary', 'btn-outline-secondary');
        tag.classList.add('btn-outline-secondary');
    });
    Array.from(BTN_TAGS)
        .filter((tag) => tags.includes(tag.value))
        .forEach(tag => {
            tag.classList.remove('btn-outline-secondary')
            tag.classList.add('btn-primary')
        });
}

function toggleTag(tag) {
    if (!tag) return;
    tag.classList.toggle('btn-outline-secondary');
    tag.classList.toggle('btn-primary');
}

async function save() {
    const tags = Array
        .from(document.querySelectorAll('.tag.btn-primary'))
        .map(t => t.value);
    if (tags.length) {
        const res = await fetch(
            `/save?index=${index.value}&image=${image.value}&tags=${tags}`);
        const data = await res.json();
        next(data);
    } else {
        alert('You must set at least one tag before saving !')
    }
}

function next({ next_index, next_image, next_tags }) {
    index.value = next_index;
    image.value = next_image;
    show.src = `/images/${next_image}`;
    initTags(next_tags);
}
