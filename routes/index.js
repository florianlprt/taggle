var fs = require('fs');
var express = require('express');
var router = express.Router();

/* Tags. */
const tags = fs.readFileSync('data/tags.csv', 'utf8').split(',');

/* Images. */
const images = fs.readdirSync('data/images/');

/* Base. */
const base_name = 'data/base.json';
const read_base = () => JSON.parse(fs.readFileSync(base_name));
const write_base = (data) => fs.writeFileSync(base_name, JSON.stringify(data));

/* GET gallery page. */
router.get('/', function(req, res, next) {
  // Read base
  const data = read_base();
  const tagged = Object.keys(data);
  // Render
  res.render('gallery', { images, tagged });
});

/* GET tag page. */
router.get('/tag', function(req, res, next) {
  // Query parameters
  const index = req.query.index;
  // Read base
  const data = read_base();
  const image = images[index];
  const tagged = data[image] ? data[image].split(',') : [];
  // Render
  res.render('tags', { index, image, tags, tagged });
});

/* GET save. */
router.get('/save', function(req, res, next) {
  // Query parameters
  const { index, image, tags } = req.query;
  // Update base
  const data = read_base();
  data[image] = tags;
  write_base(data);
  // Next image
  const next_index = (parseInt(index) + 1) % images.length;
  const next_image = images[next_index];
  const next_tags = data[next_image] ? data[next_image].split(',') : [];
  res.json({next_index, next_image, next_tags});
});

module.exports = router;
