# Taggle

Tag your images.

## Install

```sh
git clone https://gitlab.com/florianlprt/taggle.git
cd taggle/
npm install
npm start
# app listening on localhost:3000
```

## Setup

1) Feed `/data/tags.csv` with your tags (comma separated).
2) Feed `/data/images/` with your images.
3) Browse `http://localhost:3000`
4) Click an image to start tagging it.
5) Tags are saved in `/data/base.json`.

## Demo

![](public/videos/taggle.mp4)